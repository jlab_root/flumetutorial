# HADOOP - Storing data in HDFS using RabbitMQ, Flume and NodeJS
The Apache Hadoop software library is a framework that allows for the distributed processing
of large data sets across clusters of computers using simple programming models. It is
designed to scale up from single servers to thousands of machines, each offering local computation
and storage. Rather than rely on hardware to deliver high-availability, the library itself is designed
to detect and handle failures at the application layer, so delivering a highly-available service on top
of a cluster of computers, each of which may be prone to failures.

To learn more about HADOOP, visit http://hadoop.apache.org/

![alt text](https://bytebucket.org/jbyrneie/flumetutorial/raw/ce705707c9d8e9c412001c25858e9f61083c03c6/docs/images/hadoop.png "Hadoop")

Flume is a distributed, reliable and available service for efficiently collecting, aggregating,
and moving large amounts of log data. It has a simple and flexible architecture based on streaming data flows.
It is robust and fault tolerant with tunable reliability mechanisms and many failover and recovery mechanisms.
It uses a simple extensible data model that allows for online analytic application.

To learn more about Flume, visit http://flume.apache.org/

This tutorial describes how to store data in the Hadoop Distributed File System (HDFS) using a Hortonworks Single Node
Data Platform (HDP). The HDP is a Hortonworks Sandbox hosted on a VirtualBox VM. In addition, the Sandbox includes
management tools to enable you to view and manage the data.

## This tutorial describes how to
* Download and setup VirtualBox on MAC OSX
* Import Hortonworks Sandbox as a VM
* Install Maven to build open source Java packages
* Setup a RabbitMQ Server as an Event Source for Flume
* Setup Flume to consume messages from RabbitMQ and `sink` the data to HDFS
* Run a simple NodeJS RabbitMQ Producer to generate messages and route them to RabbitMQ
* Use HADOOP tools to `manage` data stored in HDFS
  1. HCatalog - a table and storage management layer - http://hortonworks.com/hadoop/hcatalog/
  2. Hive - facilitates querying and managing large datasets - see https://hive.apache.org/
  3. Use the Hortonworks Management tools to manage the data

## Download and setup VirtualBox

* Download VirtualBox for Mac OS X http://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html
* Run the downloaded `dmg` package
* Setup network adapters

## Import Hortonworks Sandbox 2.1
* Download the Hortonworks OVA from http://hortonworks.com/products/hortonworks-sandbox/#install
* Follow the instructions in http://hortonworks.com/wp-content/uploads/2014/04/InstallingHortonworksSandbox2_1MacUsingVB.pdf
* Start the VirtualBox
* Logon to VirtualBox from the Terminal Window `Fn+Alt+F5` and get its “external” IP Address
* ssh to the VirtualBox
```ssh
ssh root@<virtualBoxIpAddress>
example IP: 192.168.56.101
```

# Install maven
```maven
ssh root@<virtualBoxIpAddress>
cd ~
mkdir downloads
cd downloads
wget http://www.carfab.com/apachesoftware/maven/maven-3/3.2.2/binaries/apache-maven-3.2.2-bin.tar.gz
tar xvf apache-maven-3.2.2-bin.tar.gz
mv apache-maven-3.2.2 /usr/local/
export PATH=/usr/local/apache-maven-3.2.2/bin:$PATH
mvn –version
```

# Setup rabbitMQ Server
## Install
```install
ssh root@<virtualBoxIpAddress>
cd ~/downloads
Download rabbitmq-server-3.3.5-1.noarch.rpm from http://www.rabbitmq.com/install-rpm.html
rpm --import http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
yum install rabbitmq-server-3.3.5-1.noarch.rpm
```

## Start Server Automatically
```start
chkconfig rabbitmq-server on
```

## Enable the Rabbit Management Plugin
```enable
rabbitmq-plugins enable rabbitmq_management
./rabbitmq-plugins list – you should now see the plugin enabled
```

## Environment variables
Setup an environment variable for the rabbit install and put it on the PATH
```variables
set RABBIT_HOME=/usr/lib/rabbitmq
set PATH=$RABBIT_HOME/lib:$PATH
```

## Enable remote access to the Management Plugin
```remoting
Create/Edit /etc/rabbitmq/rabbitmq.config and insert
[{rabbit, [{loopback_users, []}]}].
```

## Create a virtual host
```virtual
rabbitmqctl add_vhost analytics
```

## Enable administration of the Virtual Host from Management Console
```manage
rabbitmqctl set_permissions -p analytics guest ".*" ".*" ".*"
```

## Start RabbitMQ as a background process
```start
rabbitmq-server start &
```

## Test the RabbitMQ Management console
```test
From a remote Browser access http://192.168.56.101:15672 and user:guest, password:guest
```

# Setup Flume
A Flume dataflow consists of configuring a source, a sink and a channel. In this tutorial, the source
consists of a RabbitMQ Server and the sink is the HDFS file system. The data flow works as follows

* The source consumes events delivered to it by RabbitMQ using the `RabbitMQ-Flume Plugin`
* When the Flume source receives an event, it stores it into one or more channels.
  The channel is a passive store that keeps the event until it’s consumed by a Flume sink
* The sink removes the event from the channel and stores the data in HDFS (via Flume HDFS sink)
* The source and sink within the given agent run asynchronously with the events staged in the channel.

![alt text](https://bytebucket.org/jbyrneie/flumetutorial/raw/51845769547f6a49a70898269f5335d44a622bf1/docs/images/flumeDataFlow.png "Flume Data Flow")

## Install Flume
```install
yum install -y flume
```

## Install the RabbitMQ-Flume Plugin
```plugin
cd ~\git
git clone https://github.com/jcustenborder/flume-ng-rabbitmq.git
mvn package
mkdir /usr/lib/flume/plugins.d/flume-rabbitmq/lib
cp target/flume-rabbitmq-channel-1.0-SNAPSHOT.jar /usr/lib/flume/plugins.d/flume-rabbitmq/lib/.
```

## Configure Flume to source from Rabbit and sink to HDFS
Setup a Flume source to consumer messages from RabbitMQ and sync the data to HDFS.

edit/create /etc/flume/conf/analytics.conf and enter the following
```configure

analytics.sources = rabbitmq_source1
analytics.channels = file_channel
analytics.sinks = sink_to_hdfs

# Define Rabbit Source
analytics.sources.rabbitmq_source1.type = org.apache.flume.source.rabbitmq.RabbitMQSource
analytics.sources.rabbitmq_source1.hostname = 192.168.56.101
analytics.sources.rabbitmq_source1.queuename = analytics
analytics.sources.rabbitmq_source1.username = guest
analytics.sources.rabbitmq_source1.password = guest
analytics.sources.rabbitmq_source1.port = 5672
analytics.sources.rabbitmq_source1.virtualhost = analytics

# HDFS sinks
analytics.sinks.sink_to_hdfs.type = hdfs
analytics.sinks.sink_to_hdfs.hdfs.fileType = DataStream
analytics.sinks.sink_to_hdfs.hdfs.path = /flume/analytics
analytics.sinks.sink_to_hdfs.hdfs.filePrefix = analytics
analytics.sinks.sink_to_hdfs.hdfs.fileSuffix = .log
analytics.sinks.sink_to_hdfs.hdfs.batchSize = 1000

# Use a channel which buffers events in memory
analytics.channels.file_channel.type = file
analytics.channels.file_channel.checkpointDir = /var/flume/checkpoint
analytics.channels.file_channel.dataDirs = /var/flume/data

# Bind the source and sink to the channel
analytics.sources.rabbitmq_source1.channels = file_channel
analytics.sinks.sink_to_hdfs.channel = file_channel
```

## Start the flume service as a background process
```start
flume-ng agent -c /etc/flume/conf -f /etc/flume/conf/analytics.conf -n analytics &
```

## View the Data
Create a Hive Table to access the HDFS data. The LOCATION value is the path specified
in the HDFS sink `analytics.sinks.sink_to_hdfs.hdfs.path` in $FLUME_HOME/conf/analytics.conf
```table
hcat -e "CREATE TABLE analytics(date STRING, ip STRING, user STRING, asset STRING) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' LOCATION '/flume/analytics/';"
```
Use the Hortonworks Management console to view the list of tables, you should now see a table called `analytics`
```hcat
http://192.168.56.101:8000/hcatalog/
```
![alt text](https://bytebucket.org/jbyrneie/flumetutorial/raw/82f67a1ac2c2d2592c3db092f4d7a0a6106200b0/docs/images/tables.png "Hive Tables")

```beeswax
Select the Beeswax Hive (UI) icon
http://192.168.56.101:8000/beeswax/

Execute a query to view the `analytics` table
select * from analytics

The results will be empty

```
![alt text](https://bytebucket.org/jbyrneie/flumetutorial/raw/82f67a1ac2c2d2592c3db092f4d7a0a6106200b0/docs/images/HiveNoData.png "No Results")


## Install and run the NodeJS Message Producer

If you do not have NodeJS installed, checkout http://shapeshed.com/setting-up-nodejs-and-npm-on-mac-osx for
instructions

On your MAC, checkout the code from git.
```checkout
git clone https://bitbucket.org/jbyrneie/flumetutorial
npm install
```

Edit etc/vars.env - this script setups ENV variables to connect to the RabbitMQ Server on
the Hortonworks Sandbox and sepcifies the queue to be created and the Virtual host to access.

When configured, source the ENV variables as follows

```setup
. etc/vars.env
```
Run the NodeJS script. The script connects to the RabbitMQ Server (specified in the `RABBIT_SERVER Environment Variable`),
creates a queue (specified in the `RABBIT_QUEUE Environment Variable`) and produces messages.

The Flume service consumes the messages and stores them in HDFS.
```run
node rabbit.js <#numMessages>
```

## View Flume Processing
You can view Flume processing progress by viewing the logs as follows

```Logs
tail -f /var/log/flume/flume.log
```


## View the Data
Select the Beeswax Hive (UI) icon in the Hortonworks Management to view the data in the `analytics` table

```view
http://192.168.56.101:8000/beeswax/

Execute a query to view the `analytics` table
select * from analytics

You should now see a set of results similar to the image below

```
![alt text](https://bytebucket.org/jbyrneie/flumetutorial/raw/82f67a1ac2c2d2592c3db092f4d7a0a6106200b0/docs/images/HiveData.png "Results")
