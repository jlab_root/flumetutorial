var amqp = require('amqp');
var moment = require('moment');

var conParam = {
  host: process.env.RABBIT_SERVER,
  port: process.env.RABBIT_PORT,
  vhost: process.env.RABBIT_VHOST
}

var connection = amqp.createConnection(conParam);
var howMany = 1;

if (typeof process.argv[2] !== 'undefined' && process.argv[2]) {
  howMany = parseInt(process.argv[2], 10);

  if (isNaN(howMany) || (howMany <= 0)) {
    console.log('%s is not a number > 0', process.argv[2]);
    process.exit(1);
  }
}

connection.on('ready', function() {

  connection.queue(process.env.RABBIT_QUEUE, {'durable': true}, function(q) {

    for (i = 0; i < howMany; i++) {
      var message = moment().format('YYYY-MM-DD:HH:mm:ss')
      + ' | ' + randomIps[random(0,randomIps.length)]
      + ' | ' + randomUsers[random(0,randomUsers.length)]
      + ' | ' + randomAssets[random(0,randomAssets.length)];

      connection.publish(process.env.RABBIT_QUEUE, message);
      console.log('Published: %s to %s@%s:%s vhost:%s ', message, process.env.RABBIT_QUEUE, process.env.RABBIT_SERVER, process.env.RABBIT_PORT, process.env.RABBIT_VHOST);
    }
  });
});

var randomIps = ['10.123.10.1',
                 '10.123.10.2',
                 '10.123.10.3',
                 '10.123.10.4',
                 '10.123.10.5',
                 '10.123.10.6',
                 '10.123.10.7',
                 '10.123.10.8',
                 '10.123.10.9'
                ];

var randomUsers = ['jbloggsIE',
                 'jbloggsUS',
                 'jbloggsDE',
                 'jbloggsFR',
                 'jbloggsUK'
                ];

var randomAssets = ['Home Page',
                 'Login',
                 'About Us',
                 'Contact Us',
                 'Checkout',
                 'Shopping',
                 'Careers'
                ];

function random (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}
